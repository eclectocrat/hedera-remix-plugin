import {
	AccountBalanceQuery,
	AccountId,
	Client,
	ContractCreateTransaction,
	ContractFunctionParameters,
	ContractId,
	FileCreateTransaction,
	FileId,
	Hbar,
	PrivateKey,
} from '@hashgraph/sdk';
import { AbiInput, AbiItem } from 'web3-utils';

// TODO: use proper contract based programming lib (pardon the pun :S)
// import { assert } from 'console';
function assert(condition: boolean, msg: string) {
	if (!condition) {
		throw Error(msg);
	}
}

export type Network = 'testnet' | 'mainnet';

export type Contract = {
	id: ContractId;
	address: string;
};

export class Hedera {
	account?: AccountId;

	privateKey?: PrivateKey;

	client?: Client;

	public init(network: Network, accountStr: string, privateKeyStr: string): void {
		this.account = AccountId.fromString(accountStr);
		this.privateKey = PrivateKey.fromString(privateKeyStr);
		if (network === 'testnet') {
			this.client = Client.forTestnet();
			this.client!.setOperator(this.account!, this.privateKey);
		}
	}

	public static abiParametersToHederaParameters(
		abi: AbiItem | null,
		abiParams: { [key: string]: string }
	): ContractFunctionParameters {
		const contractParams = new ContractFunctionParameters();
		return abi && abi!.inputs
			? abi!.inputs!.reduce((params: ContractFunctionParameters, item: AbiInput) => {
					// TODO: I ran out of time and put this horrible hack in here: I understand that it is dangerous.
					// Will have to figure out mapping for AbiItems -> ContractFunctionParameters later. Sorry :S
					const isArray = item.type.endsWith('[]');
					const dynamicFunctionName = `add${
						item.type.charAt(0).toUpperCase() + item.type.slice(1, isArray ? -2 : undefined)
					}${isArray ? 'Array' : ''}`;
					// eslint-disable-next-line
					eval(`params.${dynamicFunctionName}(${JSON.stringify(abiParams[item.name])})`);
					return params;
			  }, contractParams)
			: contractParams;
	}

	private getHedera() {
		assert(this.account !== null, 'this.account must be non-null, please ensure you call `init` before this function.');
		assert(
			this.privateKey !== null,
			'this.privateKey must be non-null, please ensure you call `init` before this function.'
		);
		assert(this.client != null, 'this.client must be non-null, please ensure you call `init` before this function.');
		return { account: this.account!, privateKey: this.privateKey!, client: this.client! };
	}

	public async uploadContract(contractDataStr: string, maxTransactionFee?: Hbar): Promise<FileId | null> {
		assert(contractDataStr !== '', 'contractDataStr must be non-null.');
		const { privateKey, client } = this.getHedera();
		console.log(client);
		const fileCreateTx = new FileCreateTransaction()
			.setContents(contractDataStr)
			.setKeys([privateKey])
			.setMaxTransactionFee(maxTransactionFee || new Hbar(1))
			.freezeWith(client);
		const fileCreateSign = await fileCreateTx.sign(privateKey);
		const fileCreateSubmit = await fileCreateSign.execute(client);
		const fileCreateRx = await fileCreateSubmit.getReceipt(client);
		return fileCreateRx.fileId;
	}

	public async createContract(bytecodeFileId: string, params: ContractFunctionParameters): Promise<Contract> {
		assert(bytecodeFileId !== '', 'bytecodeFileId must be non-null.');
		const { client } = this.getHedera();
		const contractInstantiateTx = new ContractCreateTransaction()
			.setBytecodeFileId(bytecodeFileId)
			.setGas(100000)
			.setConstructorParameters(params);

		const contractInstantiateSubmit = await contractInstantiateTx.execute(client);
		const contractInstantiateRx = await contractInstantiateSubmit.getReceipt(client);
		const { contractId } = contractInstantiateRx;
		return { id: contractInstantiateRx.contractId!, address: contractId!.toSolidityAddress() };
	}

	public async getBalance(): Promise<Hbar> {
		if (!this.account || !this.client) {
			return new Hbar(0);
		}
		const { account, client } = this.getHedera();
		const getBalanceQuery = await new AccountBalanceQuery().setAccountId(account).execute(client);
		return getBalanceQuery.hbars;
	}
}
