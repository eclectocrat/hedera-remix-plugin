import React, { useEffect } from 'react';
import { Container, Form, FormControl, InputGroup } from 'react-bootstrap';
// TODO: Add copying...?
// import copy from 'copy-to-clipboard';

import { PluginClient } from '@remixproject/plugin';
import { createClient } from '@remixproject/plugin-webview';
import { Hedera } from './hedera';
import Compiler from './components/Compiler';
import SmartContracts from './components/SmartContracts';
import { InterfaceContract } from './components/Types';

const supportedNetworks: string[] = ['Testnet', 'Mainnet'];

const App: React.FunctionComponent = () => {
	const [account, setAccount] = React.useState<string>('');
	const [publicKey, setPublicKey] = React.useState<string>('');
	const [privateKey, setPrivateKey] = React.useState<string>('');
	// TODO: regexp public key?
	// const [publicKeyError, setPublicKeyError] = React.useState<string>('');
	const [privateKeyError, setPrivateKeyError] = React.useState<string>('');

	const [balance, setBalance] = React.useState<string>('');
	const [network, setNetwork] = React.useState<string>(supportedNetworks[0]);
	const [busy, setBusy] = React.useState<boolean>(false);
	const [contracts, setContracts] = React.useState<InterfaceContract[]>([]);
	const [atAddress, setAtAddress] = React.useState<string>('');
	const [selected, setSelected] = React.useState<InterfaceContract | null>(null);

	const hederaRef = React.useRef<Hedera>(new Hedera());

	// Only once at component mount:
	useEffect(() => {
		async function tryLoadHederaEnv() {
			const client: PluginClient = createClient(new PluginClient());
			await client.onload();
			try {
				// TODO: make name of env file configurable.
				const envFile = await client.call('fileManager', 'readFile', 'browser/.env.development');
				const env = envFile
					.split('\n')
					.filter((s) => s.length > 0 && !s.trim().startsWith('#'))
					.reduce((dict, line) => {
						const [key, val] = line.split('=').map((s) => s.trim());
						return { ...dict, ...{ [key]: val } };
					}, {} as any);
				if (env.HEDERA_ACCOUNT) {
					setAccount(env.HEDERA_ACCOUNT);
				}
				if (env.HEDERA_PUBLICKEY) {
					setPublicKey(env.HEDERA_PUBLICKEY);
				}
				if (env.HEDERA_PRIVATEKEY) {
					setPrivateKey(env.HEDERA_PRIVATEKEY);
				}
			} catch (err) {
				console.info(`Unable to read Hedera '.env.development' file: ${err.message}`);
			}
		}
		tryLoadHederaEnv();
	}, []);

	// Everytime network credentials change:
	useEffect(() => {
		async function initHedera() {
			console.log(`Initializing Hedera client...`);
			setBusy(true);
			setBalance('');
			try {
				await hederaRef.current.init('testnet', account, privateKey);
				const accountBalance = await hederaRef.current.getBalance();
				setBalance(accountBalance.toString());
			} catch (err) {
				if (err.name === 'BadKeyError') {
					setPrivateKeyError(err.message);
				} else {
					console.log(err);
				}
			}
			setBusy(false);
		}
		if (account && publicKey && privateKey) {
			initHedera();
		}
	}, [account, publicKey, privateKey]);

	async function updateBalance() {
		const accountBalance = await hederaRef.current.getBalance();
		setBalance(accountBalance.toString());
	}

	function addNewContract(contract: InterfaceContract) {
		setContracts(contracts.concat([contract]));
	}

	function isNetwork(text: string): string | undefined {
		if (supportedNetworks.includes(text)) {
			return text as string;
		}
		throw new Error('This is not a valid network');
	}

	function Networks() {
		return (
			<Form.Group>
				<Form.Text className="text-muted">
					<small>NETWORK</small>
				</Form.Text>
				<InputGroup.Append>
					<Form.Control
						as="select"
						size="lg"
						value={network}
						onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
							const selectedNetwork = isNetwork(event.target.value);
							if (selectedNetwork) {
								setBalance('');
								setNetwork(selectedNetwork);
								if (selectedNetwork === 'Testnet') {
									updateBalance();
								}
							}
						}}
					>
						{supportedNetworks.map((opt) => {
							return <option key={opt}>{opt}</option>;
						})}
					</Form.Control>
					{/* TODO: Add support for hashconnect wallets. 
                    { network !== 'testnet' ?
                        <InputGroup.Append>
                            <OverlayTrigger
                                placement="bottom"
                                overlay={
                                    <Tooltip id="overlay-connect" hidden={account !== ''}>
                                        Connect to Wallet
                                    </Tooltip>
                                }
                            >
                                <Button variant="warning" block size="sm" disabled={busy} onClick={() => connect(network)}>
                                    <small>Connect</small>
                                </Button>
                            </OverlayTrigger>
                        </InputGroup.Append> :
                        <></>
                    }
                    */}
				</InputGroup.Append>
			</Form.Group>
		);
	}

	return (
		<div className="App">
			<Container>
				<Form>
					<Form.Group>
						<Networks />
					</Form.Group>
					{network === 'Testnet' ? (
						<>
							<Form.Group>
								<p className="text-center mt-3">
									<small style={{ color: 'green' }}>Operating on {network}</small>
								</p>
							</Form.Group>
							<Form.Group>
								<p className="text-center mt-3">
									<small>
										<i>
											(set credentials in an <b>.env.development</b> file at the workspace root.)
										</i>
									</small>
								</p>
								<Form.Text className="text-muted">
									<small>ACCOUNT</small>
								</Form.Text>
								<InputGroup>
									<FormControl
										aria-label="Small"
										aria-describedby="inputGroup-sizing-sm"
										placeholder="HEDERA_ACCOUNT"
										onChange={(e) => setAccount(e.target.value)}
										defaultValue={account}
									/>
								</InputGroup>
								<Form.Text className="text-muted">
									<small>PUBLIC KEY</small>
								</Form.Text>
								<InputGroup>
									<FormControl
										aria-label="Public Key"
										aria-describedby="inputGroup-sizing-sm"
										placeholder="HEDERA_PUBLICKEY"
										as="textarea"
										rows={3}
										onChange={(e) => setPublicKey(e.target.value)}
										defaultValue={publicKey}
									/>
								</InputGroup>
								<Form.Text className="text-muted">
									{privateKeyError ? (
										<small style={{ color: 'red' }}>{privateKeyError}</small>
									) : (
										<small>PRIVATE KEY</small>
									)}
								</Form.Text>
								<InputGroup>
									<FormControl
										aria-label="Public Key"
										aria-describedby="inputGroup-sizing-sm"
										placeholder="HEDERA_PRIVATEKEY"
										as="textarea"
										rows={3}
										onChange={(e) => {
											setPrivateKeyError('');
											setPrivateKey(e.target.value);
										}}
										defaultValue={privateKey}
									/>
								</InputGroup>
							</Form.Group>
						</>
					) : (
						<p className="text-center mt-3">
							<small style={{ color: 'red' }}>
								Only Testnet is supported right now, Hashconnect support for Mainnet access is coming!
							</small>
						</p>
					)}
					<Form.Group>
						<Form.Text className="text-muted">
							<small>BALANCE (Hbar)</small>
						</Form.Text>
						<InputGroup>
							<Form.Control type="text" placeholder="0.0" value={balance} size="sm" readOnly />
						</InputGroup>
					</Form.Group>
				</Form>
				<hr />
				<Compiler
					hedera={hederaRef.current!}
					busy={busy}
					setBusy={setBusy}
					addNewContract={addNewContract}
					setSelected={setSelected}
					updateBalance={updateBalance}
				/>
				{/* TODO: Add manual address controls 
                <p className="text-center mt-3">
					<small>OR</small>
				</p>
				<InputGroup className="mb-3">
					<Form.Control
						value={atAddress}
						placeholder="contract address"
						onChange={(e) => {
							setAtAddress(e.target.value);
						}}
						size="sm"
						disabled={busy || account === '' || !selected}
					/>
					<InputGroup.Append>
						<OverlayTrigger
							placement="left"
							overlay={<Tooltip id="overlay-ataddresss">Use deployed Contract address</Tooltip>}
						>
							<Button
								variant="primary"
								size="sm"
								disabled={busy || account === '' || !selected}
								onClick={() => {
									setBusy(true);
									if (selected) {
										addNewContract({ ...selected, address: atAddress });
									}
									setBusy(false);
								}}
							>
								<small>At Address</small>
							</Button>
						</OverlayTrigger>
					</InputGroup.Append>
				</InputGroup> */}
				<hr />
				<SmartContracts busy={busy} setBusy={setBusy} contracts={contracts} updateBalance={updateBalance} />
			</Container>
		</div>
	);
};

export default App;
