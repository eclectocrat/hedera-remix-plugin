import React from 'react';
import { Alert, Accordion, Button, Card, Form, InputGroup } from 'react-bootstrap';
import copy from 'copy-to-clipboard';
import { CSSTransition } from 'react-transition-group';
import { AbiInput, AbiItem } from 'web3-utils';
import { InterfaceContract } from './Types';
import Method from './Method';
import './animation.css';

const EMPTYLIST = 'Currently you have no contract instances.';

const colors: { [key: string]: string } = {
	primary: '#007aa6',
	warning: '#c97539',
	danger: '#dc3545',
	lightgreen: '#a2ffb0',
	darkgreen: '#27443f',
};

function buttonVariant(stateMutability: string | undefined): string {
	switch (stateMutability) {
		case 'view':
		case 'pure':
			return 'primary';
		case 'nonpayable':
			return 'warning';
		case 'payable':
			return 'danger';
		default:
			break;
	}
	return '';
}

interface InterfaceDrawMethodProps {
	busy: boolean;
	setBusy: (state: boolean) => void;
	abi: AbiItem;
	address: string;
	updateBalance: () => void;
}

const DrawMethod: React.FunctionComponent<InterfaceDrawMethodProps> = (props) => {
	const [error, setError] = React.useState<string>('');
	const [success, setSuccess] = React.useState<string>('');
	const [receipt, setReceipt] = React.useState<string>('');
	const [args, setArgs] = React.useState<{ [key: string]: string }>({});
	const { busy, setBusy, abi, address, updateBalance } = props;

	React.useEffect(() => {
		const temp: { [key: string]: string } = {};
		abi.inputs?.forEach((element: AbiInput) => {
			temp[element.name] = '';
		});
		setArgs(temp);
	}, [abi.inputs]);

	return (
		<>
			<Method
				abi={abi}
				setArgs={(name: string, value: string) => {
					args[name] = value;
				}}
			/>
			<Alert variant="danger" onClose={() => setError('')} dismissible hidden={error === ''}>
				<small>{error}</small>
			</Alert>
			<Alert variant="success" onClose={() => setSuccess('')} dismissible hidden={success === ''}>
				<Accordion>
					<Card style={{ border: '0' }}>
						<Accordion.Toggle
							style={{ backgroundColor: colors.darkgreen }}
							as={Card.Header}
							eventKey={success}
							className="p-1  custom-select"
						>
							Tx Receipt
						</Accordion.Toggle>
						<Accordion.Collapse style={{ backgroundColor: colors.darkgreen }} eventKey={success}>
							<Card.Body className="py-1 px-2">
								<small>{success}</small>
							</Card.Body>
						</Accordion.Collapse>
					</Card>
				</Accordion>
			</Alert>
			<InputGroup className="mb-3">
				<InputGroup.Prepend>
					<Button
						variant={buttonVariant(abi.stateMutability)}
						block
						size="sm"
						disabled // TODO: ={busy}
						onClick={async () => {
							setBusy(true);
							// TODO
							setBusy(false);
						}}
					>
						<small>{abi.stateMutability === 'view' || abi.stateMutability === 'pure' ? 'call' : 'transact'}</small>
					</Button>
					<Button
						variant={buttonVariant(abi.stateMutability)}
						size="sm"
						className="mt-0 pt-0 float-right"
						onClick={() => {
							// TODO
						}}
					>
						<i className="far fa-copy" />
					</Button>
				</InputGroup.Prepend>
				<Form.Control
					value={receipt}
					size="sm"
					readOnly
					hidden={!(abi.stateMutability === 'view' || abi.stateMutability === 'pure')}
				/>
			</InputGroup>
		</>
	);
};

const ContractCard: React.FunctionComponent<{
	busy: boolean;
	setBusy: (state: boolean) => void;
	contract: InterfaceContract;
	index: number;
	remove: () => void;
	updateBalance: () => void;
}> = (props) => {
	const [enable, setEnable] = React.useState<boolean>(true);
	const { busy, setBusy, contract, index, remove, updateBalance } = props;

	function DrawMethods(indexOfContract: number) {
		const list = contract.abi ? contract.abi : [];
		const items = list.map((abi: AbiItem, id: number) => (
			<Accordion key={`Methods_A_${indexOfContract.toString()}_${id.toString()}`}>
				<Card>
					<Accordion.Toggle
						style={{ color: 'white', backgroundColor: colors[buttonVariant(abi.stateMutability)] }}
						as={Card.Header}
						eventKey={`Methods_${indexOfContract.toString()}_${id.toString()}`}
						className="p-1  custom-select"
					>
						<small>{abi.name}</small>
					</Accordion.Toggle>
					<Accordion.Collapse eventKey={`Methods_${indexOfContract.toString()}_${id.toString()}`}>
						<Card.Body className="py-1 px-2">
							<DrawMethod
								busy={busy}
								setBusy={setBusy}
								abi={abi}
								address={contract.address}
								updateBalance={updateBalance}
							/>
						</Card.Body>
					</Accordion.Collapse>
				</Card>
			</Accordion>
		));
		return <>{items}</>;
	}

	return (
		<CSSTransition in={enable} timeout={300} classNames="zoom" unmountOnExit onExited={remove}>
			<Card className="mb-2">
				<Accordion.Toggle as={Card.Header} eventKey="0" className="px-2 py-1 form-control custom-select">
					<strong className="align-middle">{contract.name}</strong>
					&nbsp;
					<small className="align-middle">{`${contract.address.substring(0, 6)}...${contract.address.substring(
						38
					)}`}</small>
					<Button
						variant="link"
						size="sm"
						className="float-left align-middle"
						onClick={() => {
							copy(contract.address);
						}}
					>
						<i className="far fa-copy" />
					</Button>
					<Button
						className="float-left align-middle"
						size="sm"
						variant="link"
						onClick={() => {
							setEnable(false);
						}}
					>
						<i className="fas fa-trash-alt" />
					</Button>
				</Accordion.Toggle>
				<Accordion.Collapse eventKey="0">
					<Card.Body>{DrawMethods(index)} </Card.Body>
				</Accordion.Collapse>
			</Card>
		</CSSTransition>
	);
};

interface InterfaceSmartContractsProps {
	busy: boolean;
	setBusy: (state: boolean) => void;
	contracts: InterfaceContract[];
	updateBalance: () => void;
}

const SmartContracts: React.FunctionComponent<InterfaceSmartContractsProps> = (props) => {
	const [error, setError] = React.useState<string>('');
	const [count, setCount] = React.useState<number>(0);
	const { busy, setBusy, contracts, updateBalance } = props;

	React.useEffect(() => {
		setCount(0);
		setError(EMPTYLIST);
	}, [contracts, busy]);

	function DrawContracts() {
		const items = contracts.map((data: InterfaceContract, index: number) => (
			<Accordion defaultActiveKey={index.toString()}>
				<ContractCard
					busy={busy}
					setBusy={setBusy}
					contract={data}
					index={index}
					remove={() => {
						setCount(count + 1);
						setError(EMPTYLIST);
					}}
					updateBalance={updateBalance}
					key={`Contract_${index.toString()}`}
				/>
			</Accordion>
		));
		return <div>{items}</div>;
	}

	return (
		<div className="SmartContracts">
			<Alert variant="warning" className="text-center" hidden={contracts.length !== count}>
				<small>{error}</small>
			</Alert>
			{DrawContracts()}
		</div>
	);
};

export default SmartContracts;
