# Hedera Plugin for Remix

### See `post-hackathon-cleanup` for additional changes since the Hedera Hackathon close.

The Hedera plugin for Ethereum's Remix IDE: deploy on the Hedera testnet.

Based on Moonbeam Remix Plugin: https://github.com/purestake/moonbeam-remix-plugin

# Getting Started

First, clone the repository:
```
git clone https://gitlab.com/eclectocrat/hedera-remix-plugin
cd hedera-remix-plugin
```

Install dependencies:
```
yarn install
```

Run the app:
```
yarn start
``` 

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits, open the console to see int errors and warnings.

## View Local Changes on Remix

When developing the plugin, you can view the changes you're making locally from within Remix by taking the following steps:

1. From Remix, click on the **Plugin Manager** icon on the left side menu
2. Towards the top, click on **Connect to a Local Plugin**
3. Fill in the plugin details:
    - **Plugin Name** - Hedera Test Plugin
    - **Url** - http://localhost:3000
    - **Type of connection** - iframe
    - **Location in remix** - side panel
4. Click on **OK**
5. A question mark icon should appear on the left side menu for the local plugin

Once the local plugin has been created, you can navigate to it test out your changes!

## How to Publish a Constract on Hedera Testnet

1. Click RemixIDE Plugin Manager Icon and activate Hedera Plugin
2. Click Hedera Plugin icon
4. Select network (right now only testnet is supported)
3. Enter credentials info, or add a `.env.development` file with credentials to the root of the workspace
5. Compile your smart contract
6. Select contract to deploy
7. Configure constructor parameters, if any
8. Click Deploy
